package main

import (
	"log"
	"math"
	"net/http"
	"time"
)

func RunService() {
	s := &http.Server{
		Addr:         "172.22.51.15:8080",
		Handler:      Router{},
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	log.Fatal(s.ListenAndServe())
}
type Operation struct {
	Operator string `json:"operator"`
	Value    int    `json:"value"`

}
type Calculator interface {
	Calculate(op Operation) int
}
type sum struct {}

func (a sum) Calcule(op Operation) int {
	return op.Value1 + op.Value2
}
type sub struct {}

func (s sub) Calculate(op Operation) int {
	return op.Value1 - op.Value2
}
type mul struct {}

func (m mul) Calculate(op Operation) int {
	return op.Value1 * op.Value2
}
type div struct {}

func (d div) Calculate(op Operation) int {
	if op.Value2 != 0 {
	   return op.Value1 / Value2
	}else {
	   return 0
	}
}
type pow struct {}

func (p pow) Calculate(op Opertation) int {
	return int(math.Pow(float64(op.Value1), float64(op.Value2)))
}

type rot struct {}

func (r Srqt) Calculate(op Operation) int {
	return int(math.Sqrt(float64(op.Value1), 1.0/float64(op.Value2)))
}
var operations = map[string]Calculator{
	"+": sum{},
	"sum": sum{},
	"-": sub{},
	"sub": sub{},
	"*": mul{},
	"mul": mul{},
	"/": div{},
	"div": div{},
	"^": pow{},
	"pow": pow{},
	"&": rot{},
	"rot": rot{},
}